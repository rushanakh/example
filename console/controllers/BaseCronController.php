<?php

namespace console\controllers;


use frontend\models\CronInfo;
use yii\console\Controller;

class BaseCronController extends Controller
{
	/**
	 * Выводить ли вывод (устанавливать в дочерних классах по желанию)
	 *
	 * @var bool
	 */
	protected $_verbose = false;

	/**
	 * @param \yii\base\Action $action
	 * @param array            $params
	 *
	 * @return array
	 */
	public function bindActionParams($action, $params)
	{
		if (in_array('verbose', $params)) {
			$this->_verbose = true;
		}

		return parent::bindActionParams($action, $params);
	}

	/**
	 * Лочим повторный запуск скрипта
	 *
	 * @param $name
	 *
	 * @return bool
	 */
	protected function _lock($name) {
		$lock = sys_get_temp_dir() . '/' . YII_ENV . "_$name.lock";
		$aborted = file_exists($lock) ? filemtime($lock) : null;
		$fp = fopen($lock, 'w');
		if (!flock($fp, LOCK_EX|LOCK_NB)) {
			// заблокировать файл не удалось, значит запущена копия скрипта
			return false;
		}
		// получили блокировку файла

		// если файл уже существовал значит предыдущий запуск кто-то прибил извне
		if ($aborted) {
			//error_log(sprintf("Запуск скрипта %s был завершен аварийно %s", $name, date('c', $aborted)));
		}

		// снятие блокировки по окончанию работы
		// если этот callback, не будет выполнен, то блокировка
		// все равно будет снята ядром, но файл останется
		register_shutdown_function(function() use ($fp, $lock) {
			flock($fp, LOCK_UN);
			fclose($fp);
			unlink($lock);
		});

		return true;
	}

	/**
	 * Вывод
	 *
	 * @param string|array $text
	 */
	protected function _echo($text) {
		if (is_array($text)) {
			$text = print_r($text, true);
		}
		if ($this->_verbose) {
			echo PHP_EOL . date('d.m.Y H:i:s: ') . $text;
		}
	}

	/**
	 * Возвращает CronInfo для текущего крона
	 *
	 * @return CronInfo
	 */
	protected function _getCronInfo()
	{
		$actionId = \Yii::$app->controller->action->id;
		$cronInfo = CronInfo::findOne(['name' => $actionId]);
		if (!$cronInfo) {
			$cronInfo = new CronInfo(['name' => $actionId]);
		}

		return $cronInfo;
	}
}