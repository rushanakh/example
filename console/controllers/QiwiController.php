<?php

namespace console\controllers;

use common\helpers\BillingHelper;
use common\helpers\QiwiHelper;
use common\library\qiwi\QiwiBill;
use common\library\qiwi\QiwiResponse;
use frontend\models\Payment;

/**
 * Class QiwiController
 * @package console\controllers
 */
class QiwiController extends BaseCronController
{
	/**
	 * Обходит все оплаты в статусе "ожидает", и запрашивает по ним информацию
	 * php yii qiwi/check-bills verbose
	 *
	 * @throws \yii\base\ErrorException
	 */
	public function actionCheckBills()
	{
		$payments = Payment::find()
						  ->byStatus(Payment::STATUS_WAIT)
						  ->all();
		
		$qiwiClient = QiwiHelper::getQiwiClient();
		$billingHelper = new BillingHelper();
		
		$this->_echo('Найдено счетов в ожидании: ' . count($payments));
		
		foreach ($payments as $payment) {
			$this->_echo("Запрашиваем информацию по счету #$payment->payment_id - $payment->source_txn_id");
			$qiwiResponse = $qiwiClient->billInfo($payment->source_txn_id);
			
			if ($qiwiResponse->result_code == QiwiResponse::RESULT_CODE_SUCCESS && $qiwiResponse->bill) {
				switch ($qiwiResponse->bill->status) {
					case QiwiBill::STATUS_PAID:
						$this->_echo("\tСчет оплачен");
						//зачисляем
						$billingHelper->acceptPayment($payment);
					break;
					case QiwiBill::STATUS_WAITING:
						$this->_echo("\tОжидание оплаты счета");
					break;
					case QiwiBill::STATUS_REJECTED:
						$this->_echo("\tСчет отклонен");
						//отменяем оплату
						$billingHelper->cancelPayment($payment);
					break;
					case QiwiBill::STATUS_UNPAID:
						$this->_echo("\tОшибка при проведении оплаты. Счет не оплачен");
						//отменяем оплату
						$billingHelper->cancelPayment($payment);
					break;
					case QiwiBill::STATUS_EXPIRED:
						$this->_echo("\tВремя жизни счета истекло. Счет не оплачен");
						//отменяем оплату
						$billingHelper->cancelPayment($payment);
					break;
				}
			} else {
				$this->_echo("\tНе получили данные по счету");
			}
		}
		
		$this->_echo("\tЗакончили");

	}
}