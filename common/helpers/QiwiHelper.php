<?php

namespace common\helpers;
use common\library\qiwi\Qiwi;
use common\library\qiwi\QiwiResponse;
use yii\helpers\Url;

/**
 * Class QiwiHelper
 *
 * @package common\helpers
 */
class QiwiHelper
{
	/**
	 * @return Qiwi
	 * @throws \Exception
	 */
	public static function getQiwiClient()
	{
		return (new Qiwi(
			\Yii::$app->params['qiwiShopId'],
			\Yii::$app->params['qiwiRestId'],
			\Yii::$app->params['qiwiRestPass'],
			\Yii::$app->params['qiwiProviderName']
		));
	}
	
	/**
	 * @param integer $phone
	 * @param float $amount
	 * @param string $billId
	 * @param string $comment
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public static function createBill($phone, $amount, $billId, $comment)
	{
		$phoneNumber = preg_replace("/[^0-9]/", '', $phone);
		$qiwi    = self::getQiwiClient();
		$iso8601 = date('c', time() + \Yii::$app->params['qiwiBillLifeTime']);
		$qiwiResponse = $qiwi->createBill($phoneNumber, $amount, $iso8601, $billId, $comment);
		
		if ($qiwiResponse->result_code == QiwiResponse::RESULT_CODE_SUCCESS) {
			echo $qiwi->redirect(
				$billId,
				Url::to(['/qiwi/check-payment', 'billId' => $billId], true),
				Url::to(['/qiwi/check-payment', 'billId' => $billId], true)
			);
			exit;
		}

		return false;
	}
}