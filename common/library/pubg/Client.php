<?php

namespace common\library\pubg;

use common\models\pubg\PubgApiLog;
use yii\base\BaseObject;
use yii\web\HttpException;

class Client
{
	private $__host = "https://api.pubg.com";
	
	private $__apiKey = null;
	private $__platformRegion = self::PLATFORM_REGION_PC_RU;
	
	const PLATFORM_STEAM           = 'steam';
	const PLATFORM_XBOX            = 'xbox';
	const PLATFORM_KAKAO           = 'kakao';
	const PLATFORM_REGION_XBOX_AS  = 'xbox-as';
	const PLATFORM_REGION_XBOX_EU  = 'xbox-eu';
	const PLATFORM_REGION_XBOX_NA  = 'xbox-na';
	const PLATFORM_REGION_XBOX_OC  = 'xbox-oc';
	const PLATFORM_REGION_PC_KRJP  = 'pc-krjp';
	const PLATFORM_REGION_PC_JP    = 'pc-jp';
	const PLATFORM_REGION_PC_NA    = 'pc-na';
	const PLATFORM_REGION_PC_EU    = 'pc-eu';
	const PLATFORM_REGION_PC_RU    = 'pc-ru';
	const PLATFORM_REGION_PC_OC    = 'pc-oc';
	const PLATFORM_REGION_PC_KAKAO = 'pc-kakao';
	const PLATFORM_REGION_PC_SEA   = 'pc-sea';
	const PLATFORM_REGION_PC_SA    = 'pc-sa';
	const PLATFORM_REGION_PC_AS    = 'pc-as';
	
	public static $regionForStats = [
			self::PLATFORM_STEAM,
			self::PLATFORM_REGION_PC_RU,
			self::PLATFORM_REGION_PC_AS,
			self::PLATFORM_REGION_PC_EU,
			self::PLATFORM_REGION_PC_NA,
			self::PLATFORM_REGION_PC_JP,
			self::PLATFORM_REGION_PC_KAKAO,
			self::PLATFORM_REGION_PC_KRJP,
			self::PLATFORM_REGION_PC_SEA,
			self::PLATFORM_REGION_PC_SA,
			self::PLATFORM_REGION_PC_OC,
	];
	
	/**
	 * Client constructor.
	 *
	 * @param string $apiKey
	 * @param string $platformRegion
	 */
	public function __construct(string $apiKey, string $platformRegion = self::PLATFORM_STEAM)
	{
		$this->__apiKey = $apiKey;
		$this->__platformRegion = $platformRegion;
	}

	/**
	 * Доступные шарды (регионы) для выбора людьми
	 *
	 * @param bool $appendCode - добавлять ли в скобках код региона (типа pc-na)
	 *
	 * @return array
	 */
	public static function getAvailableRegions($appendCode = false)
	{
		$regions = [
			self::PLATFORM_STEAM           => 'Steam',
			self::PLATFORM_XBOX            => 'Xbox',
			self::PLATFORM_KAKAO           => 'Kakao',
			self::PLATFORM_REGION_PC_RU    => 'Россия',
			self::PLATFORM_REGION_PC_AS    => 'Азия',
			self::PLATFORM_REGION_PC_EU    => 'Европа',
			self::PLATFORM_REGION_PC_NA    => 'Северная Америка',
			self::PLATFORM_REGION_PC_JP    => 'Япония',
			self::PLATFORM_REGION_PC_KAKAO => 'Корея',
			self::PLATFORM_REGION_PC_KRJP  => 'Корея и Япония',
			self::PLATFORM_REGION_PC_SEA   => 'Юго-восточная Азия',
			self::PLATFORM_REGION_PC_SA    => 'Южная и Центральная Америка',
			self::PLATFORM_REGION_PC_OC    => 'Океания',
		];
		if ($appendCode) {
			foreach ($regions as $region => $text) {
				$regions[$region] = $text .' ('.$region.')';
			}
		}

		return $regions;
	}
	
	/**
	 * @return null
	 */
	public function getApiKey()
	{
		return $this->__apiKey;
	}
	
	/**
	 * @param null $_apiKey
	 */
	public function setApiKey($_apiKey): void
	{
		$this->__apiKey = $_apiKey;
	}
	
	/**
	 * @return null
	 */
	public function getPlatformRegion()
	{
		return $this->__platformRegion;
	}
	
	/**
	 * @param null $_platformRegion
	 */
	public function setPlatformRegion($_platformRegion): void
	{
		$this->__platformRegion = $_platformRegion;
	}
	
	/**
	 * @param bool $withShard
	 *
	 * @return string
	 */
	public function getUrl():string
	{
		$urlArray = [
			$this->__host,
			'shards',
			$this->__platformRegion
		];
		
		return implode('/', $urlArray) . "/";
	}
	
	/**
	 * @return string
	 */
	public function getSeasons(): string
	{
		$url = $this->getUrl() . "seasons";
		
		return $this->__send($url, true);
	}
	
	/**
	 * @param string $accountId
	 * @param string $seasonId
	 *
	 * @return string
	 */
	public function getSeasonStats(string $accountId, string $seasonId): string
	{
		if (empty($accountId) || empty($seasonId)) {
			return "";
		}
		
		$urlArray = [
			'players',
			$accountId,
			'seasons',
			$seasonId
		];
		
		$url =  $this->getUrl() . implode('/', $urlArray);
		
		return $this->__send($url, true);
	}
	
	/**
	 * @param array $playerNames
	 *
	 * @return string
	 * @throws \HttpException
	 */
	public function getPlayersByName(array $playerNames): string
	{
		if (empty($playerNames)) {
			return "";
		}
		
		$url = $this->getUrl() . "players?filter[playerNames]=" . implode(',', $playerNames);
		
		return $this->__send($url, true);
	}
	
	/**
	 * @param array $playerIds
	 *
	 * @return string
	 * @throws \HttpException
	 */
	public function getPlayersById(array $playerIds): string
	{
		if (empty($playerIds)) {
			return "";
		}
		
		$url = $this->getUrl() . "players?filter[playerIds]=" . implode(',', $playerIds);
		
		return $this->__send($url, true);
	}
	
	/**
	 * @param string $playerId
	 *
	 * @return string
	 * @throws \HttpException
	 */
	public function getPlayerById(string $playerId): string
	{
		if (empty($playerId)) {
			return "";
		}
		
		$url = $this->getUrl() . "players/" . $playerId;
		
		return $this->__send($url, true);
	}
	
	/**
	 * @param string $matchId
	 *
	 * @return string
	 * @throws \HttpException
	 */
	public function getMatchById(string $matchId): string
	{
		if (empty($matchId)) {
			return "";
		}
		
		$url = $this->getUrl() . "matches/" . $matchId;
		
		return $this->__send($url);
	}
	
	/**
	 * @param      $url
	 * @param bool $rateLimit
	 *
	 * @return string
	 */
	private function __send($url, $rateLimit = false): string
	{
		$options = [
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_HEADER => false,
			CURLOPT_HTTPHEADER => [
				"Authorization: Bearer " . $this->__apiKey,
				"Accept: application/vnd.api+json"
			],
		];
		
		$ch = curl_init($url);
		
		curl_setopt_array($ch, $options);
		
		if (!curl_errno($ch)) {
			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			if ($httpCode != 200) {
				throw new HttpException($httpCode);
			}
		}
		
		$result = curl_exec($ch);
		
		curl_close($ch);
		
		return $result;
	}
	
	/**
	 * @return bool
	 */
	private function __canSendRequest():bool
	{
		$pubgApiLogs = PubgApiLog::find()->byCreatedAt(time()-PubgApiLog::API_REQUEST_LIMIT_INTERVAL)->sortByCreatedAt(SORT_DESC)->count();
		if ($pubgApiLogs <= PubgApiLog::API_REQUEST_LIMIT) {
			return true;
		}
		
		return false;
	}
}