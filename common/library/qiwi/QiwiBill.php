<?php

namespace common\library\qiwi;

use yii\base\Model;

class QiwiBill extends Model
{
	const STATUS_WAITING  = 'waiting';	// Счет выставлен, ожидает оплаты или оплачивается
	const STATUS_PAID     = 'paid';		// Счет оплачен
	const STATUS_REJECTED = 'rejected';	// Счет отклонен
	const STATUS_UNPAID   = 'unpaid';	// Ошибка при проведении оплаты. Счет не оплачен
	const STATUS_EXPIRED  = 'expired';	// Время жизни счета истекло. Счет не оплачен
	
	const COMMAND_BILL = 'bill';
	
	/** @var string */
	public $bill_id;
	
	/** @var float */
	public $amount;
	
	/** @var string */
	public $ccy;
	
	/** @var string */
	public $status;
	
	/** @var integer */
	public $error;
	
	/** @var string */
	public $comment;
	
	/** @var string  */
	public $command;
	
	/** @var string */
	public $user;
	
	/** @var string */
	public $prv_name;
}