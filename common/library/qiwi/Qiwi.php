<?php

namespace common\library\qiwi;

use Exception;

class Qiwi
{
	const STATUS_WAITING  = 'waiting';	// Счет выставлен, ожидает оплаты или оплачивается
	const STATUS_PAID     = 'paid';		// Счет оплачен
	const STATUS_REJECTED = 'rejected';	// Счет отклонен
	const STATUS_UNPAID   = 'unpaid';	// Ошибка при проведении оплаты. Счет не оплачен
	const STATUS_EXPIRED  = 'expired';	// Время жизни счета истекло. Счет не оплачен
	
	/**
	 * Если способ оплаты не доступен, пользователю
	 * отображается предупреждение, при этом на странице
	 * можно выбрать другие способы оплаты.
	 */
	const PAY_SOURCE_QW     = 'qw';		// оплата с баланса Visa QIWI Wallet
	const PAY_SOURCE_MOBILE = 'mobile';	// оплата с баланса мобильного телефона
	const PAY_SOURCE_CARD   = 'card';	// оплата банковской картой
	const PAY_SOURCE_WM     = 'wm';		// оплата с привязанного кошелька WebMoney
	const PAY_SOURCE_SSK    = 'ssk';	// оплата наличными в терминале QIWI
	
	const ERROR_CODE_SUCCESS    = 0; //Успех
	const ERROR_CODE_FORMAT     = 5; //Ошибка формата параметров запроса
	const ERROR_CODE_DB         = 13; //Ошибка соединения с базой данных
	const ERROR_CODE_ACCESS     = 150; //Ошибка проверки пароля
	const ERROR_CODE_SIGN       = 151; //Ошибка проверки подписи
	const ERROR_CODE_CONNECTION = 300; //Ошибка связи с сервером
	
	const RESULT_CODE_SUCCESS = 0;
	const RESULT_CODES = [
		0    => 'Успех',
		5    => 'Неверные данные в параметрах запроса',
		13   => 'Сервер занят, повторите запрос позже',
		78   => 'Недопустимая операция',
		150  => 'Ошибка авторизации',
		152  => 'Не подключен или отключен протокол',
		155  => 'Данный идентификатор провайдера (API ID) заблокирован',
		210  => 'Счет не найден',
		215  => 'Счет с таким bill_id уже существует',
		241  => 'Сумма слишком мала',
		242  => 'Сумма слишком велика, или сумма, переданная в запросе возврата средств, превышает сумму самого счета либо сумму счета, оставшуюся после предыдущих возвратов',
		298  => 'Кошелек с таким номером не зарегистрирован',
		300  => 'Техническая ошибка',
		303  => 'Неверный номер телефона',
		316  => 'Попытка авторизации заблокированным провайдером',
		319  => 'Нет прав на данную операцию',
		339  => 'Ваш IP-адрес или массив адресов заблокирован',
		341  => 'Обязательный параметр указан неверно или отсутствует в запросе',
		700  => 'Превышен месячный лимит на операции',
		774  => 'Кошелек временно заблокирован',
		1001 => 'Запрещенная валюта для провайдера',
		1003 => 'Не удалось получить курс конвертации для данной пары валют',
		1019 => 'Не удалось определить сотового оператора для мобильной коммерции',
		1419 => 'Нельзя изменить данные счета – он уже оплачивается или оплачен',
	];
	
	/**
	 * ID магазина
	 * @var int
	 */
	public $shopId;
	
	/**
	 * API ID (REST ID) для BASIC авторизации
	 * @var int
	 */
	public $restId;
	
	/**
	 * пароль API
	 * @var string
	 */
	public $restPass;
	
	/**
	 * валюта
	 * @var string
	 */
	public $currency = 'RUB';
	
	/**
	 * Источник оплаты
	 * @var string
	 */
	public $paySource = self::PAY_SOURCE_QW;
	
	/**
	 * Название провайдера
	 * @var string
	 */
	public $providerName;
	
	/**
	 * Отладка
	 * @var boolean
	 */
	public $debug = false;
	
	/**
	 * @param int $shopId
	 * @param int $restId
	 * @param string $restPass
	 * @param string $providerName
	 *
	 * @throws Exception
	 */
	public function __construct($shopId, $restId, $restPass, $providerName)
	{
		$this->shopId = $shopId;
		$this->restId = $restId;
		$this->restPass = $restPass;
		$this->providerName = $providerName;
		
		if (!function_exists('curl_init'))
		{
			throw new Exception('CURL library not found on this server');
		}
	}
	
	/**
	 * @param string $url
	 *
	 * @return resource
	 */
	private function __curlInit($url)
	{
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		
		return $ch;
	}
	
	/**
	 * Выставление счета
	 *
	 * @param string $tel 		Телефон пользователя, на которого выставляется счет
	 * @param int    $amount 	Сумма счета
	 * @param string $date 		Срок годности счета (в формате ISO 8601)
	 * @param string $billId 	Уникальный номер счета
	 * @param string $comment 	Комментарий к платежу (не обязательно)
	 *
	 * @return QiwiResponse|null 			Объект ответа от сервера QIWI
	 *
	 * @throws Exception
	 */
	public function createBill($tel, $amount, $date, $billId, $comment = null)
	{
		$parameters = [
			'user'       => 'tel:+' . $tel, // телефон начинается с +
			'amount'     => $amount,
			'ccy'        => $this->currency,
			'comment'    => $comment,
			'pay_source' => $this->paySource,
			'lifetime'   => $date,
			'prv_name'   => $this->providerName,
		];
		
		$ch = $this->__curlInit('https://w.qiwi.com/api/v2/prv/' . $this->shopId . '/bills/' . $billId);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			"Accept: text/json",
			"Content-Type: application/x-www-form-urlencoded; charset=utf-8"
		]);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_USERPWD, $this->restId . ':' . $this->restPass);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parameters));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		$httpResponse = curl_exec($ch);
		
		if ($this->debug) {
			var_dump($httpResponse);
		}
		if (!$httpResponse)
		{
			// Описание ошибки, к примеру
			throw new Exception(curl_error($ch) . '(' . curl_errno($ch) . ')');
			
			return false;
		}
		$httpResponseAr = @json_decode($httpResponse);
		
		return $httpResponseAr->response;
	}
	
	/**
	 * Возвращает ссылку на страницу оплаты счета.
	 *
	 * @param string $billId 		Уникальный номер счета
	 * @param string $successUrl 	URL, на который пользователь будет переброшен в случае успешного проведения операции (не обязательно)
	 * @param string $failUrl 		URL, на который пользователь будет переброшен в случае неудачного завершения операции (не обязательно)
	 *
	 * @return string 				Ссылка на страницу оплаты счета
	 */
	public function payLink($billId, $successUrl = '', $failUrl = '')
	{
		return "https://w.qiwi.com/order/external/main.action?shop=" . $this->shopId . "&transaction=" . $billId .
			   "&successUrl=" . $successUrl . "&failUrl=" . $failUrl;
	}
	
	/**
	 * Переадресация на страницу оплаты счета
	 *
	 * @param string $billId 		Уникальный номер счета
	 * @param string $successUrl 	URL, на который пользователь будет переброшен в случае успешного проведения операции (не обязательно)
	 * @param string $failUrl 		URL, на который пользователь будет переброшен в случае неудачного завершения операции (не обязательно)
	 */
	public function redirect($billId, $successUrl = '', $failUrl = '')
	{
		header("Location: " . $this->payLink($billId, $successUrl, $failUrl));
	}
	
	/**
	 * Информация о счете
	 *
	 * @param string $billId 	Уникальный номер счета
	 *
	 * @return QiwiResponse 	Объект ответа от сервера QIWI
	 *
	 * @throws Exception
	 */
	public function billInfo($billId)
	{
		$ch = $this->__curlInit('https://w.qiwi.com/api/v2/prv/' . $this->shopId . '/bills/' . $billId);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			"Accept: text/json",
			"Content-Type: application/x-www-form-urlencoded; charset=utf-8"
		]);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_USERPWD, $this->restId . ':' . $this->restPass);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		$httpResponse = curl_exec($ch);
		if ($this->debug)
		{
			var_dump($httpResponse);
		}
		if (!$httpResponse)
		{
			// Описание ошибки, к примеру
			throw new Exception(curl_error($ch) . '(' . curl_errno($ch) . ')');
			
			return false;
		}
		$httpResponseAr = @json_decode($httpResponse);
		
		return $httpResponseAr->response;
	}
	
	/**
	 * Отмена платежа
	 *
	 * @param string $billId Уникальный номер счета
	 *
	 * @return QiwiResponse
	 * @throws Exception
	 */
	public function rejectBill($billId)
	{
		$parameters = [
			'status' => 'rejected'
		];
		$ch         = $this->__curlInit('https://w.qiwi.com/api/v2/prv/' . $this->shopId . '/bills/' . $billId);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			"Accept: text/json",
			"Content-Type: application/x-www-form-urlencoded; charset=utf-8"
		]);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_USERPWD, $this->restId . ':' . $this->restPass);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parameters));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		$httpResponse = curl_exec($ch);
		if ($this->debug)
		{
			var_dump($httpResponse);
		}
		if (!$httpResponse)
		{
			// Описание ошибки, к примеру
			throw new Exception(curl_error($ch) . '(' . curl_errno($ch) . ')');
			
			return false;
		}
		$httpResponseAr = @json_decode($httpResponse);
		
		return $httpResponseAr->response;
	}
}