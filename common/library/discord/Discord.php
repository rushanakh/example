<?php

namespace common\library\discord;

use Exception;

class Discord
{

	const DISCORD_LOGIN_URL = 'https://discordapp.com/api/oauth2/authorize?';
	const DISCORD_TOKEN_URL = 'https://discordapp.com/api/oauth2/token?';
	const DISCORD_API_URL = 'https://discordapp.com/api';
	const RESPONSE_TYPE = 'code';
	const SCOPE = 'identify';
	const GRANT_TYPE = 'authorization_code';

	/**
	 * Authorization Code от Discord-сервера
	 *
	 * @var int
	 */
	public $code;

	/**
	 * Id Discord-приложения
	 *
	 * @var int
	 */
	public $clientId;

	/**
	 * Секретный ключ Discord-приложения
	 *
	 * @var int
	 */
	public $clientSecret;

	/**
	 * Адрес колбэка
	 *
	 * @var int
	 */
	public $callbackUrl;

	/**
	 *  Тип авторизации
	 *
	 * @var int
	 */
	public $grant_type;

	/**
	 * Полномочия
	 *
	 * @var int
	 */
	public $scope;

	/**
	 * Отладка
	 *
	 * @var boolean
	 */
	public $debug = false;

	/**
	 * Discord constructor.
	 *
	 * @param int $code
	 *
	 * @throws Exception
	 */
	public function __construct ($code = null)
	{
		$params = \Yii::$app->params['integration']['discord'];

		$this->clientId = $params['clientId'];
		$this->clientSecret = $params['clientSecret'];
		$this->callbackUrl = $params['callbackUrl'];
		$this->grant_type = self::GRANT_TYPE;
		$this->scope = self::SCOPE;
		$this->code = $code;

		if (!function_exists('curl_init')) {
			throw new Exception('CURL library not found on this server');
		}
	}

	/**
	 *
	 * Получаем токен
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function getToken ()
	{
		$payload = [
			'client_id'     => $this->clientId,
			'client_secret' => $this->clientSecret,
			'grant_type'    => $this->grant_type,
			'code'          => $this->code,
			'redirect_uri'  => $this->callbackUrl,
			'scope'         => $this->scope
		];

		$curl_options = [
			CURLOPT_URL            => self::DISCORD_TOKEN_URL,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => http_build_query($payload),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_TIMEOUT        => 30
		];

		$curl = curl_init();
		curl_setopt_array($curl, $curl_options);
		$httpResponse = json_decode(curl_exec($curl), true);

		if ($this->debug) {
			var_dump($httpResponse);
		}
		if (!$httpResponse) {
			throw new Exception(curl_error($curl) . '(' . curl_errno($curl) . ')');
			return false;
		}
		curl_close($curl);

		return $httpResponse['access_token'];
	}

	/**
	 *
	 * Получаем профайл текущего пользователя Disord
	 *
	 * @param $access_token
	 *
	 * @return bool|mixed
	 * @throws Exception
	 */
	public function getCurrentDiscordUserData ($access_token)
	{
		$curl_options = [
			CURLOPT_URL            => self::DISCORD_API_URL . '/users/@me',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTPHEADER     => [
				"Authorization: Bearer {$access_token}"
			],
			CURLOPT_TIMEOUT        => 30
		];

		$curl = curl_init();
		curl_setopt_array($curl, $curl_options);
		$httpResponse = json_decode(curl_exec($curl), true);

		if ($this->debug) {
			var_dump($httpResponse);
		}
		if (!$httpResponse) {
			throw new Exception(curl_error($curl) . '(' . curl_errno($curl) . ')');
			return false;
		}
		curl_close($curl);

		return $httpResponse;
	}
}