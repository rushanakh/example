<?php
namespace frontend\models;

use common\helpers\Crypt;
use common\helpers\RBACHelper;
use common\models\AuthAssignment;
use frontend\models\battle\Battle;
use frontend\models\integration\AccountData;
use frontend\models\integration\IntegrationBattleNetBasic;
use frontend\models\integration\IntegrationDiscord;
use frontend\models\integration\IntegrationSteam;
use frontend\models\match\Match;
use frontend\models\match\MatchParticipant;
use frontend\models\match\MatchQuery;
use libphonenumber\PhoneNumberUtil;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property UserProfile          $userProfile
 * @property UserIntegration      $userIntegration
 * @property UserIntegration[]    $userIntegrationsSimple
 * @property UserDocument         $userDocument
 * @property Game[]               $games
 * @property UserGame[]           $userGames
 * @property UserGame             $userGame
 * @property Game                 $currentGame
 * @property Account              $account
 * @property UserNotification     $userNotifications
 * @property AccountData|null     $twitchIntegration
 * @property IntegrationSteam     $steamIntegration
 * @property IntegrationSteam[]   $steamIntegrations
 * @property UserIntegration[]    $steamUserIntegrations
 * @property User                 $referralUser
 * @property Match                $currentMatch
 * @property Match                $currentGameMatch
 * @property Match                $tournamentMatch
 * @property UserLastActivityInfo $userLastActivityInfo
 * @property UtmTag[]             $utmTags
 * @property string               $phoneFormatted
 * @property TeamInvitation[]     $teamInvitations
 * @property AuthAssignment[]     $authAssignment
 *
 * @property int                  $user_id
 * @property int                  $is_admin
 * @property string               $auth_key
 * @property string               $password_hash
 * @property string               $password_reset_token
 * @property string               $email                        почта
 * @property string               $email_confirm_token
 * @property string               $nick_name
 * @property int                  $is_adult
 * @property int                  $status                       Код статуса пользователя
 * @property int                  $state                        Сейчас играет
 * @property int                  $ready                        Готов играть
 * @property string               $phone
 * @property int                  $is_email_verified            Подтверждённость почты
 * @property int                  $is_phone_verified            Подтверждённость телефона
 * @property string               $ip                           IP
 * @property int                  $current_game_id              ID текущей выбранной игры
 * @property int                  $referral_id                  ID пригласившего юзера, если пришёл по приглащению
 * @property int                  $date_created
 * @property int                  $date_updated
 */
class User extends ActiveRecord implements IdentityInterface
{
	const STATUS_DELETED = 0;
	const STATUS_BLOCKED = 20;
	const STATUS_ACTIVE = 30;
	
	const STATE_NONE        = 0; // ничего не делает
	const STATE_NOW_PLAYING = 10; // сейчас играет
	
	const STATE_NAME_READY_ON = 'ready-on';
	const STATE_NAME_READY_PLAY = 'ready-play';
	const STATE_NAME_READY_OFF = 'ready-off';

	const EMAIL_IS_VERIFIED = 1;
	const EMAIL_IS_NOT_VERIFIED = 0;
	
	const ONLINE_DELAY = 300;//сколько секунд должно пройти с последней активности, прежде чем мы посчитаем пользователя офлайн

	public $sessionSmsCodeParam = 'smsCodeValue';
	public $smsCodeTimeout = 60;
	
	public $stateNameTitle = [
		self::STATE_NAME_READY_ON => 'Готов играть',
		self::STATE_NAME_READY_PLAY => 'Сейчас играю',
		self::STATE_NAME_READY_OFF => 'Не готов играть'
	];

	/** @var  UserGame[] */
	private $__userGame = [];
	private $__currentUserGameId;

	/**
	 * @var Match
	 */
	private $__currentMatch = null;

	/**
	 * @var Match
	 */
	private $__currentGameMatch = null;

	/**
	 * @var Match
	 */
	private $__currentTournamentMatch = null;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'users';
	}

	/**
	 * @inheritdoc
	 */
	public function beforeValidate()
	{
		$this->is_phone_verified = (int)$this->is_phone_verified;
		$this->is_email_verified = (int)$this->is_email_verified;

		return parent::beforeValidate();
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			[
				'class'              => TimestampBehavior::className(),
				'createdAtAttribute' => 'date_created',
				'updatedAtAttribute' => 'date_updated',
			]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['auth_key', 'password_hash', 'status', 'nick_name'], 'required'],
			[['status', 'referral_id', 'is_admin', 'is_adult', 'ready', 'state'], 'integer'],
			['status', 'default', 'value' => self::STATUS_ACTIVE],
			['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_BLOCKED, self::STATUS_DELETED]],
			[['auth_key', 'password_hash'], 'string', 'max' => 128],
			[['password_reset_token', 'email_confirm_token', 'email'], 'string', 'max' => 255],
			[['phone'], 'string', 'max' => 50],
			[['nick_name'], 'string', 'max' => 255],
			['nick_name', 'match', 'pattern' => '/@/', 'not' => true, 'message' => 'Знак @ запрещен в имени.'],
		];
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nick_name' => 'Имя',
            'email' => 'Email',
            'phone' => 'Телефон',
            'date_created' => 'Дата регистрации',
            'account.balance' => 'Банкнот',
            'account.tickets' => 'Монет',
        ];
    }

    /**
	 * Получает текущего юзера, если залогинен
	 *
	 * @return User|null|IdentityInterface
	 */
	public static function getCurrentUser()
	{
		return \Yii::$app->user && !\Yii::$app->user->isGuest
			? \Yii::$app->user->identity
			: null;
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentity($id)
	{
		return static::findOne(['user_id' => $id, 'status' => [self::STATUS_ACTIVE]]);
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token, $type = null)
	{
		throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
	}

	/**
	 * Finds user by password reset token
	 *
	 * @param string $token password reset token
	 * @return static|null
	 */
	public static function findByPasswordResetToken($token)
	{
		if (!static::isPasswordResetTokenValid($token)) {
			return null;
		}

		return static::findOne([
			'password_reset_token' => $token,
			'status' => self::STATUS_ACTIVE,
		]);
	}

	/**
	 * Finds out if password reset token is valid
	 *
	 * @param string $token password reset token
	 * @return bool
	 */
	public static function isPasswordResetTokenValid($token)
	{
		if (empty($token)) {
			return false;
		}

		$timestamp = (int) substr($token, strrpos($token, '_') + 1);
		$expire = Yii::$app->params['user.passwordResetTokenExpire'];
		return $timestamp + $expire >= time();
	}

	/**
	 * @param string $emailConfirmToken
	 *
	 * @return static|null
	 */
	public static function findByEmailConfirmToken($emailConfirmToken)
	{
		return static::findOne(['email_confirm_token' => $emailConfirmToken, 'is_email_verified' => 0]);
	}

	/**
	 * Generates email confirmation token
	 */
	public function generateEmailConfirmToken()
	{
		$this->email_confirm_token = Yii::$app->security->generateRandomString();
	}

	/**
	 * Removes email confirmation token
	 */
	public function removeEmailConfirmToken()
	{
		$this->email_confirm_token = null;
	}


	/**
	 * Отправка письма для подтверждения e-mail
	 */
	public function sendEmailForConfirm()
	{
		try {
			Yii::$app->mailer
				->compose(
					[
						'html' => '@app/views/mail_templates/email_confirm_html.php',
						'text' => '@app/views/mail_templates/email_confirm_text.php'
					],
					['user' => $this]
				)
				->setFrom([Yii::$app->params['noreplyEmail'] => Yii::$app->params['noreplyName']])
				->setTo($this->email)
				->setSubject('Подтверждение e-mail для ' . Yii::$app->name)
				->send();
		} catch (\Exception $e) {
			die($e->getMessage());
		}
	}

	/**
	 * @inheritdoc
	 */
	public function getId()
	{
		return $this->getPrimaryKey();
	}

	/**
	 * @inheritdoc
	 */
	public function getAuthKey()
	{
		return $this->auth_key;
	}

	/**
	 * @inheritdoc
	 */
	public function validateAuthKey($authKey)
	{
		return $this->getAuthKey() === $authKey;
	}

	/**
	 * Validates password
	 *
	 * @param string $password password to validate
	 * @return bool if password provided is valid for current user
	 */
	public function validatePassword($password)
	{
		return Yii::$app->security->validatePassword($password, $this->password_hash);
	}

	/**
	 * Generates password hash from password and sets it to the model
	 *
	 * @param string $password
	 */
	public function setPassword($password)
	{
		$this->password_hash = Yii::$app->security->generatePasswordHash($password);
	}

	/**
	 * Generates "remember me" authentication key
	 */
	public function generateAuthKey()
	{
		$this->auth_key = Yii::$app->security->generateRandomString();
	}

	/**
	 * Generates new password reset token
	 */
	public function generatePasswordResetToken()
	{
		$this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
	}

	/**
	 * Removes password reset token
	 */
	public function removePasswordResetToken()
	{
		$this->password_reset_token = null;
	}

	/**
	 * @param $email
	 *
	 * @return null|static
	 */
	public static function findByEmail($email)
	{
		return self::findOne(['email' => $email]);
	}

	/**
	 * @param $phone
	 *
	 * @return null|static
	 */
	public static function findByPhone($phone)
	{
		return self::findOne(['phone' => self::getClearPhone($phone)]);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUserProfile()
	{
		return $this->hasOne(UserProfile::className(), ['user_id' => 'user_id']);
	}

	public function setUserProfile()
	{
		$this->userProfile->user_id = $this->user_id;
		return $this->userProfile->save();
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUserDocument()
	{
		return $this->hasOne(UserDocument::className(), ['user_id' => 'user_id']);
	}

	/**
	 * Возвращает информацию по интеграции
	 *
	 * @param int $gameId
	 *
	 * @return array|UserIntegration|null
	 */
	public function getUserIntegrationByGame(int $gameId)
	{
		return UserIntegration::find()
			->byUserId($this->user_id)
			->byGameId($gameId)
			->one();
	}

	/**
	 * Привязан ли у юзера аккаунт интеграции по этой игре (если не передан gameId, то по current_game_id)
	 *
	 * @param int $gameId
	 *
	 * @return AccountData|false
	 * @throws \Exception
	 */
	public function getIntegrationDataByGame(int $gameId = null)
	{
		$gameId = $gameId ?? $this->current_game_id;

		if (!$gameId) {
			return false;
		}

		$integration = $this->getUserIntegrationByGame($gameId);
		if (!$integration) {
			return false;
		}
		$integrationModel = $integration->getIntegrationModel();

		if (empty($integrationModel)){
			return false;
		}

		$api = $integrationModel->getApi();

		return $api->getAccountData()->hasAnyData() ? $api->getAccountData() : false;
	}

	/**
	 * Возвращает аватарку из интеграции по игре (если есть) или с площадки (если нет в интеграции)
	 * Во втором случае вернёт указанного в imgSize размере
	 *
	 * @param int    $gameId
	 * @param string $imgSize
	 *
	 * @return null|string
	 */
	public function getGameImageByGame(int $gameId, $imgSize = UserProfile::IMG_SIZE_MICRO)
	{
		if ($gameId && $this->getIntegrationDataByGame($gameId)) {
			if ($avatar = $this->getIntegrationDataByGame($gameId)->getPureAvatar()) {
				return $avatar;
			}
		}

		return $this->userProfile->getImage($imgSize);
	}

	/**
	 * Возвращает имя юзера из интеграции по игре (если есть) или на площадке
	 * Если appendSiteName is truthy, то присобачит в скобках имя на площадке (если есть интеграционное)
	 *
	 * @param int  $gameId
	 * @param bool $appendSiteName
	 *
	 * @return string
	 */
	public function getGameNameByGame(int $gameId, $appendSiteName = false)
	{
		if ($gameId && $this->getIntegrationDataByGame($gameId)) {
			if ($name = $this->getIntegrationDataByGame($gameId)->getName()) {
				return $appendSiteName
					? $name .' ('.$this->nick_name.')'
					: $name;
			}
		}

		return $this->nick_name;
	}

	/**
	 * Возвращает public рейтинг в игре или 0, ежели нет такого
	 *
	 * @param int $gameId
	 *
	 * @return int
	 */
	public function getGameRatingByGame(int $gameId)
	{
		return ($gameId && $this->getUserGameByGameId($gameId) &&  $this->getUserGameByGameId($gameId)->userGameRating)
			? $this->getUserGameByGameId($gameId)->userGameRating->getPublicRating()
			: 0;
	}

	/**
	 * Получает данные по привязанному twitch-каналу или null, если не привязан
	 *
	 * @return AccountData|null
	 */
	public function getTwitchIntegration()
	{
		$twitchIntegration = UserIntegration::find()
		                                    ->byIntegrationId(Integration::TWITCH)
		                                    ->byUserId($this->user_id)
		                                    ->one();
		if ($twitchIntegration) {
			$model = $twitchIntegration->getIntegrationModel();
			if ($model && $api = $model->getApi()) {
				$data = $api->getAccountData();
				if ($data->hasAnyData()) {
					return $data;
				}
			}
		}

		return null;
	}

	/**
	 * @return array|IntegrationDiscord|null
	 */
	public function getDiscordIntegration ()
	{
		$discordIntegration = UserIntegration::find()
			->byIntegrationId(Integration::DISCORD)
			->byUserId($this->user_id)
			->one();

		if ($discordIntegration) {
			$userIntegrationId = $discordIntegration->user_integration_id;
			$discordAccount = IntegrationDiscord::find()
				->byUserIntegrationId($userIntegrationId)
				->one();

			return $discordAccount;
		}

		return null;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGames()
	{
		return $this->hasMany(Game::className(), ['game_id' => 'game_id'])
			->viaTable('user_games', ['user_id' => 'user_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUserGames()
	{
		return $this->hasMany(UserGame::className(), ['user_id' => 'user_id'])->indexBy('game_id');
	}

	/**
	 * Возвращает игру по game_id
	 *
	 * @param int $gameId
	 *
	 * @return null|UserGame
	 */
	public function getUserGameByGameId($gameId)
	{
		if (!isset($this->__userGame[$gameId])) {
			$this->__userGame[$gameId] = UserGame::findOne(['user_id' => $this->user_id, 'game_id' => $gameId]);
		}

		return $this->__userGame[$gameId];
	}

	/**
	 * Активируем пользователя
	 *
	 * @return bool
	 */
	public function activate()
	{
		$this->is_email_verified = self::EMAIL_IS_VERIFIED;
		return $this->save();
	}
	
	/**
	 * Установка статуса
	 *
	 * @param int $state
	 *
	 * @return bool
	 */
	public function setState($state = self::STATE_NONE)
	{
		if ($state == self::STATE_NONE) {
			$activeBattle = Battle::find()
								  ->byMatchStatus(Match::STATUS_RUNNING)
								  ->byUser($this)
								  ->limit(1)
								  ->one();
			if (!$activeBattle) {
				$this->state = self::STATE_NONE;
			} else {
				$this->state = self::STATE_NOW_PLAYING;
			}
		} else {
			$this->state = self::STATE_NOW_PLAYING;
		}
		
		return $this->save();
	}
	
	/**
	 * Получаем статус
	 *
	 * @return bool
	 */
	public function getStateName()
	{
		if ($this->ready) {
			switch ($this->state) {
				case User::STATE_NONE:
					return self::STATE_NAME_READY_ON;
				break;
				case User::STATE_NOW_PLAYING:
					return self::STATE_NAME_READY_PLAY;
				break;
				default:
					return self::STATE_NAME_READY_ON;
				break;
			}
		} else {
			return self::STATE_NAME_READY_OFF;
		}
	}
	
	/**
	 * Получаем статус для title
	 *
	 * @param string $stateName
	 *
	 * @return mixed|null
	 */
	public function getStateTitle($stateName)
	{
		if (isset($this->stateNameTitle[$stateName])) {
			return $this->stateNameTitle[$stateName];
		}
		return null;
	}

	/**
	 * @param $phone
	 */
	public function setPhone($phone)
	{
		$this->phone = self::getClearPhone($phone);
	}

	/**
	 * Выкидываем из строки телефона всё кроме цифр
	 *
	 * @param $phone
	 *
	 * @return string
	 */
	public static function getClearPhone($phone)
	{
		$phone = preg_replace('/[^0-9]/', '', $phone);
		if ($phone == '7') {
			$phone = '';
		}
		return $phone;
	}

	/**
	 * @return ActiveQuery|TeamInvitationQuery
	 */
	public function getTeamInvitations()
	{
		return $this->hasMany(TeamInvitation::class, ['user_id' => 'user_id']);
	}

	/**
	 * Получаем командs игрока по игре (если не передана, то по current_game_id юзера)
	 *
	 * @param int $gameId
	 *
	 * @return Team|null
	 */
	public function getCurrentGameTeam($gameId = null)
	{
		if (!$gameId) {
			$gameId = $this->current_game_id;
		}
		$userGame = $this->getUserGameByGameId($gameId);
		if (!$userGame) {
			return null;
		}
		
		$team = Team::find()
					->byGameId($gameId)
					->byUserGameId($userGame->user_game_id)
					->limit(1)
					->one();
		
		return $team;
	}

	/**
	 *
	 * Получаем все команды игрока по игре (если не передана, то по current_game_id юзера)
	 *
	 * @param null $gameId
	 *
	 * @return array|Team[]|TeamUser[]|null
	 */
	public function getCurrentGameTeams($gameId = null)
	{
		if (!$gameId) {
			$gameId = $this->current_game_id;
		}
		$userGame = $this->getUserGameByGameId($gameId);
		if (!$userGame) {
			return null;
		}

		$teams = Team::find()
			->byGameId($gameId)
			->byUserGameId($userGame->user_game_id)
			->all();

		return $teams;
	}

	/**
	 * @return TeamQuery
	 */
	public function getTeams()
	{
		return Team::find()->byUserGameId(
			$this->getUserGames()->select('user_game_id')->column()
		);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCurrentGame()
	{
		return $this->hasOne(Game::className(), ['game_id' => 'current_game_id']);
	}
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAuthAssignment()
	{
		return $this->hasOne(AuthAssignment::class, ['user_id' => 'user_id'])->indexBy('item_name');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAccount()
	{
		return $this->hasOne(Account::className(), ['entity_id' => 'user_id'])
					->andWhere(['entity_type' => Account::TYPE_USER]);
	}

	/**
	 * Проверяем может ли пользователь создавать новые команды
	 * todo определить когда можно, когда нельзя.
	 *
	 * @return bool
	 */
	public function canCreateTeam()
	{
		return true;
	}

	/**
	 * ID юзера криптованный (для реф. ссылки, например)
	 *
	 * @return string
	 */
	public function getEncryptedId()
	{
		return Crypt::get()->encrypt($this->user_id);
	}

	/**
	 * Возвращает кол-во приведённых текущим юзером новых юзеров
	 *
	 * @return int
	 */
	public function getReferredCount()
	{
		return (int)User::find()
			->where(['referral_id' => $this->user_id]) // , 'status' => User::STATUS_ACTIVE
			->count(); //
	}

	/**
	 * Пользователь, пригласивший на портал
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getReferralUser()
	{
		return $this->hasOne(self::className(), ['user_id' => 'referral_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUserNotifications()
	{
		return $this->hasMany(UserNotification::className(), ['user_id' => 'user_id'])->orderBy('notification_id DESC');
	}

	/**
	 * @inheritdoc
	 * @return UserQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new UserQuery(get_called_class());
	}

	/**
	 * Связь с интеграцией по игре, текущей для текущего пользователя
	 */
	public function getUserIntegration()
	{
		$gameId = $this->getCurrentWebUserGameId() ?? $this->current_game_id;
		return $this->hasOne(UserIntegration::className(), ['user_id' => 'user_id'])->where(['game_id' => $gameId]);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getSteamUserIntegrations()	{
		return $this->hasMany(UserIntegration::className(), ['user_id' => 'user_id'])->where(['integration_id' => Integration::STEAM]);
	}
	
	
	/**
	 * @return ActiveQuery|UserIntegrationQuery
	 */
	public function getUserIntegrationsSimple()
	{
		return $this->hasMany(UserIntegration::class, ['user_id' => 'user_id']);
	}

	/**
	 * @return AccountData|null
	 */
	public function getIntegrationData()
	{
		return !empty($this->userIntegration) ? $this->userIntegration->accountData : null;
	}

	/**
	 * Возвращает аватарку из интеграции по текущей игре (если есть) или с площадки (если нет в интеграции)
	 * Во втором случае вернёт указанного в imgSize размере
	 *
	 * @param string $imgSize
	 *
	 * @return null|string
	 */
	public function getGameImage($imgSize = UserProfile::IMG_SIZE_MICRO)
	{
		$integrationData = $this->getIntegrationData();
		if ($integrationData && $integrationData->getPureAvatar()) {
			return $integrationData->getPureAvatar();
		}

		return $this->userProfile->getImage($imgSize);
	}

	/**
	 * Возвращает имя юзера из интеграции по текущей игре (если есть) или на площадке
	 * Если appendSiteName is truth, то присобачит в скобках имя на площадке (если есть интеграционное)
	 *
	 * @param bool $appendSiteName
	 *
	 * @return string
	 */
	public function getGameName($appendSiteName = false)
	{
		$gameId = $this->getCurrentWebUserGameId() ?? $this->current_game_id;
		if ($gameId == Game::PUBG) {
			if ($this->userProfile && !empty($this->userProfile->pubg_nickname)) {
				$pubgNickName = $this->userProfile->pubg_nickname;
				return $appendSiteName ? $pubgNickName .' ('.$this->nick_name.')' : $pubgNickName;
			}
		}
		$integrationData = $this->getIntegrationData();
		if ($integrationData) {
			$name = $integrationData->getName();
			if ($name) {
				return $appendSiteName ? $name .' ('.$this->nick_name.')' : $name;
			}
		}

		return $this->nick_name;
	}
	
	/**
	 * @param int $gameId
	 *
	 * @return string
	 */
	public function getNickByGame($gameId)
	{
		if ($gameId == Game::PUBG) {
			if ($this->userProfile && !empty($this->userProfile->pubg_nickname)) {
				return  $this->userProfile->pubg_nickname;
			}
		}
		$currentIntegration = $userIntegration = null;
		if (!empty($this->userGames[$gameId]) && $this->userGames[$gameId]->integration && $this->userGames[$gameId]->integration->currentIntegration) {
			$currentIntegration = $this->userGames[$gameId]->integration->currentIntegration;
		}
		
		$nickname  = (!empty($currentIntegration))
			? $currentIntegration->getName()
			: $this->nick_name;
		
		return $nickname;
	}
	
	/**
	 * @param int $gameId
	 *
	 * @return null|string
	 */
	public function getAvatarByGame($gameId)
	{
		$userIntegration = null;
		if (!empty($this->userGames[$gameId]) && $this->userGames[$gameId]->integration && $this->userGames[$gameId]->integration->currentIntegration) {
			$userIntegration = $this->userGames[$gameId]->integration;
		}
		
		$avatar = (!empty($userIntegration))
			? $userIntegration->getUserAvatar(UserIntegration::AVATAR_MINI)
			: $this->userProfile->getImage(UserProfile::IMG_SIZE_MICRO);
		
		return $avatar;
	}
	
	/**
	 * Возвращает имя юзера из интеграции по текущей игре (если есть)
	 * с именем и фамилией если указаны
	 *
	 * Имя "ник из интеграции" Фамилия
	 *
	 * Например: Спиридон «Подозрительная Сова» Замухрайкин
	 *
	 * @param bool $encode
	 * @param Game|null $game
	 *
	 * @return string
	 */
	public function getFullNameWithNick($encode = true, $game = null)
	{
		if (!$game) {
			$gameId = $this->getCurrentWebUserGameId() ?? $this->current_game_id;
		}
		$gameId = $game->game_id;
		$nickByGame = $this->getNickByGame($gameId);
		if (!empty($this->userProfile->getFullName())) {
			$name =
				$this->userProfile->first_name
				. ' «' .
				$nickByGame
				. '» ' .
				$this->userProfile->last_name;
		} else {
			$name = $nickByGame;
		}
		
		$name = trim($name);

		return $encode ? Html::encode($name) : $name;
	}

	/**
	 * Возвращает public рейтинг в игре или 0, ежели нет такого
	 *
	 * @return int
	 */
	public function getGameRating()
	{
		return ($this->userGame && $this->userGame->userGameRating)
			? $this->userGame->userGameRating->getPublicRating()
			: 0;
	}

	/**
	 * Связь с текущей игрой
	 *
	 * @return ActiveQuery
	 */
	public function getUserGame()
	{
		$gameId = $this->getCurrentWebUserGameId() ?? $this->current_game_id;
		return $this->hasOne(UserGame::className(), ['user_id' => 'user_id'])->where(['game_id' => $gameId]);
	}

	/**
	 * Текущий матч игрока (идущий или тот, в котором он не указал счёт, а оппонент указал)
	 *
	 * @return Match
	 */
	public function getCurrentMatch()
	{
		if ($this->__currentMatch === null) {
			if ($participants = MatchParticipant
				::find()
				->joinWith(['match' => function (MatchQuery $query) {
					return $query->byStatuses([Match::STATUS_RESULTS, Match::STATUS_RUNNING]);
				}])
				->byUserId($this->user_id)
				->all()
			) {
				foreach ($participants as $participant) {
					if ($participant->match->status == Match::STATUS_RUNNING) {
						$this->__currentMatch = $participant->match;
						break;
					}
					if (!$participant->match->isUserVoted($this->user_id)) {
						$this->__currentMatch = $participant->match;
						break;
					}
				}
			}
		}

		return $this->__currentMatch;
	}

    /**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUserLastActivityInfo()
	{
		return $this->hasOne(UserLastActivityInfo::className(), ['user_id' => 'user_id']);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getUtmTags()
	{
		return $this->hasMany(UtmTag::className(), ['user_id' => 'user_id']);
	}

	/**
	 * Обновляем время последней активности
	 *
	 * @return bool
	 */
	public function updateActivityTime()
	{
		$activity = $this->userLastActivityInfo;
		if (empty($activity)) {
			$activity = new UserLastActivityInfo(['user_id' => $this->user_id]);
		}
		$activity->last_activity_time = time();

		return $activity->save();
	}

	/**
	 * Логика проверки на онлайн статус пользователя
	 *
	 * @return bool
	 */
	public function isOnline()
	{
		return $this->userLastActivityInfo && $this->userLastActivityInfo->last_activity_time > time() - self::ONLINE_DELAY;
	}

	/**
	 * Может ли пользователь использовать реальные деньги
	 *
	 * @return int
	 */
	public function isCanProcessMoney()
	{
		return $this->is_phone_verified;
	}

	/**
	 * Проверка на админа
	 *
	 * @return bool
	 */
	public function isAdmin()
	{
		return !empty($this->is_admin);
	}

	/**
	 * @return bool
	 */
	public function isModerator()
	{
		return \Yii::$app->authManager->checkAccess($this->user_id, RBACHelper::ROLE_MODERATOR);
	}

	/**
	 * Форматированный для человековосприятия телефон
	 *
	 * @return string
	 */
	public function getPhoneFormatted()
	{
		if ($this->phone) {
			$phoneUtil = PhoneNumberUtil::getInstance();

			return $phoneUtil->format($phoneUtil->parse($this->phone), 1);
		}

		return '-';
	}

	/**
	 * @return ActiveQuery|IntegrationSteam
	 */
	public function getSteamIntegration()
	{
		return $this->hasOne(IntegrationSteam::class, ['user_integration_id' => 'user_integration_id'])
		            ->viaTable(UserIntegration::tableName() . ' i_f_s', ['user_id' => 'user_id'],
			            function (ActiveQuery $q) {
				            return $q->andOnCondition(['i_f_s.integration_id' => Integration::STEAM]);
			            });
	}

	/**
	 * @return ActiveQuery|IntegrationSteam
	 */
	public function getSteamIntegrations()
	{
		return $this->hasMany(IntegrationSteam::class, ['user_integration_id' => 'user_integration_id'])
			->viaTable(UserIntegration::tableName() . ' i_f_s', ['user_id' => 'user_id'],
				function (ActiveQuery $q) {
					return $q->andOnCondition(['i_f_s.integration_id' => Integration::STEAM]);
				});
	}

	/**
	 * @return ActiveQuery
	 */
	public function getBattleNetIntegration()
	{
		return $this->hasOne(IntegrationBattleNetBasic::class, ['user_integration_id' => 'user_integration_id'])
		            ->viaTable(UserIntegration::tableName() . ' i_f_b', ['user_id' => 'user_id'],
			            function (ActiveQuery $q) {
				            return $q->andOnCondition(['i_f_b.integration_id' => Integration::BATTLE_NET_BASIC]);
			            });
	}

	/**
	 * текущая игра WebUser
	 */
	private function getCurrentWebUserGameId()
	{
		if (empty($this->__currentUserGameId) && !empty(Yii::$app->user->currentGameId)) {
			$this->__currentUserGameId = Yii::$app->user->currentGameId;
		}

		return $this->__currentUserGameId;
	}
}
