<?php

namespace frontend\models;

/**
 * This is the ActiveQuery class for [[User]].
 *
 * @see User
 */
class UserQuery extends \yii\db\ActiveQuery
{
	/**
	 * @param $nickName
	 */
	public function byNickName($nickName)
	{
		$this->andWhere(['nick_name' => $nickName]);
	}
	
	/**
	 * Выбираем тех кто онлайн
	 *
	 * @param bool $eager
	 *
	 * @return $this
	 */
	public function byOnline($eager = false)
	{
		return $this->joinWith(['userLastActivityInfo' => function (UserLastActivityInfoQuery $query)
		{
			$query->byOnlineUser();
		}], $eager);
	}
	
	/**
	 * Выбираем тех кто готов сейчас играть
	 *
	 * @return $this
	 */
	public function byReadyToPlay()
	{
		return $this
			->andWhere(['ready' => 1])
			->andWhere(['state' => User::STATE_NONE]);
	}
	
	/**
	 * Выбираем тех кто сейчас играет
	 *
	 * @return $this
	 */
	public function byPlayingNow()
	{
		return $this
			->andWhere(['state' => User::STATE_NOW_PLAYING]);
	}

	/**
	 * Сортировка по времени последней активности
	 *
	 * @param int $sort
	 *
	 * @return $this
	 */
	public function sortByLastActivity($sort = SORT_DESC)
	{
		return $this->joinWith(['userLastActivityInfo' => function (UserLastActivityInfoQuery $query) use ($sort)
		{
			$query->orderBy(['last_activity_time' => $sort]);
		}], false);
	}
	
	/**
	 * Выборка по игре
	 *
	 * @param int  $gameId
	 * @param bool $eager
	 *
	 * @return $this
	 */
	public function byGame($gameId, $eager = false)
	{
		return $this->joinWith(['userGames' => function (UserGameQuery $query) use ($gameId)
		{
			$query->byGameId($gameId);
		}], $eager);
	}
	
	/**
	 * Выборка по рейтингу в указанной игре, с сортировкой
	 *
	 * @param int $gameId
	 * @param int $sort
	 *
	 * @return $this
	 */
	public function byGameRating($gameId, $sort = SORT_DESC)
	{
		return $this->leftJoin('user_games', 'users.user_id = user_games.user_id')
					->leftJoin('user_game_rating', 'user_games.user_game_id = user_game_rating.user_game_id')
					->andWhere('user_games.game_id = ' . $gameId)
					->orderBy(['user_game_rating.rating' => $sort]);
	}
	
	/**
	 * Выборка по командному рейтингу игрока в указанной игре, с сортировкой
	 *
	 * @param int $gameId
	 * @param int $sort
	 *
	 * @return $this
	 */
	public function byUserGameTeamRating($gameId, $sort = SORT_DESC)
	{
		return $this->leftJoin('user_games', 'users.user_id = user_games.user_id')
					->leftJoin('user_game_rating', 'user_games.user_game_id = user_game_rating.user_game_id')
					->andWhere('user_games.game_id = ' . $gameId)
					->orderBy(['user_game_rating.team_rating' => $sort]);
	}

	/**
	 * Активные
	 *
	 * @return $this
	 */
	public function active()
	{
		return $this->andWhere(['status' => User::STATUS_ACTIVE]);
	}

	/**
	 * По рефералке
	 *
	 * @param $refId
	 *
	 * @return $this
	 */
	public function byReferral($refId)
	{
		return $this->andWhere(['referral_id' => $refId]);
	}
	
	/**
	 * @param $userId
	 *
	 * @return UserQuery
	 */
	public function byUserId($userId)
	{
		return$this->andWhere(['user_id' => $userId]);
	}

	/**
	 * @param $status
	 *
	 * @return UserQuery
	 */
	public function byStatus($status)
	{
		return$this->andWhere(['status' => $status]);
	}
	
	/**
	 * @inheritdoc
	 * @return UserGame[]|array
	 */
	public function all($db = null)
	{
		return parent::all($db);
	}

	/**
	 * @inheritdoc
	 * @return UserGame|array|null
	 */
	public function one($db = null)
	{
		return parent::one($db);
	}
}
