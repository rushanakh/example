<?php
namespace frontend\controllers;


use frontend\models\User;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Class LoggedInAbstractController
 *
 * @package frontend\controllers
 *
 * Абстрактный класс для контроллеров, обслуживающих залогиненных
 * По умолчанию всем action'ам требуется залогиненность
 * $_user содержит текущего юзера
 */
abstract class LoggedInAbstractController extends BaseController
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}
}