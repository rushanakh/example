<?php
namespace frontend\controllers;

use common\helpers\ActivityHelper;
use common\helpers\BillingHelper;
use common\helpers\Crypt;
use common\helpers\RewardHelper;
use common\helpers\TalksHelper;
use common\helpers\TeamHelper;
use common\helpers\TextHelper;
use common\library\pubg\Client;
use common\models\PayoutRequest;
use frontend\components\WebUser;
use frontend\helpers\ChangeGameHelper;
use frontend\models\forms\BankFillForm;
use frontend\models\forms\MoneyWithdrawalForm;
use frontend\models\forms\PasswordChangeForm;
use frontend\models\forms\ProfileEditForm;
use frontend\models\forms\PubgNicknameForm;
use frontend\models\forms\UploadImageForm;
use frontend\models\forms\UserProfileEditForm;
use frontend\models\Game;
use frontend\models\Integration;
use frontend\models\TeamInvitation;
use frontend\models\TeamUser;
use frontend\models\Transaction;
use frontend\models\User;
use frontend\models\UserGameQuery;
use frontend\models\UserProfile;
use Yii;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * User controller
 */
class UserController extends LoggedInAbstractController
{
	/**
	 * @return array
	 */
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['access']['rules'][] = [
			'allow' => true,
			'actions' => ['game-profile', 'choose-game', 'online-users'],
		];

		return $behaviors;
	}
	
	/**
	 * Страница с пользователями онлайн
	 *
	 * @param string $tab
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 */
	public function actionOnlineUsers($tab = 'tab-ready')
	{
		if (!$this->_game) {
			\Yii::$app->user->setReturnUrl(['user/online-users']);
			
			return $this->redirect(['user/choose-game']);
		}

		$userReadyQuery = User::find()
		             ->byGame($this->_game->game_id)
		             ->byOnline()
					 ->byReadyToPlay()
		             ->notSuperUser()
		             ->with(['userGame.userGameRating', 'userIntegration']);
		
		$userNowPlayingQuery = User::find()
					  ->byGame($this->_game->game_id)
					  ->byOnline()
					  ->notSuperUser()
					  ->byPlayingNow()
					  ->with(['userGame.userGameRating', 'userIntegration']);
		
		$userOnlineQuery = User::find()
								   ->byGame($this->_game->game_id)
								   ->byOnline()
								   ->notSuperUser()
								   ->with(['userGame.userGameRating', 'userIntegration']);

		$userReadyProvider = new ActiveDataProvider([
		   'query' => $userReadyQuery,
		   'pagination' => [
			   'pageSize' => 10,
			   'pageParam' => 'ready-page',
			   'params' => array_merge(Yii::$app->request->get(), ['tab' => 'tab-ready'])
		   ],
		]);
		
		$userNowPlayingProvider = new ActiveDataProvider([
			'query' => $userNowPlayingQuery,
			'pagination' => [
				'pageSize' => 10,
				'pageParam' => 'now-playing-page',
				'params' => array_merge(Yii::$app->request->get(), ['tab' => 'tab-now-playing'])
			],
		]);
		
		$userOnlineProvider = new ActiveDataProvider([
			 'query' => $userOnlineQuery,
			 'pagination' => [
				 'pageSize' => 100,
				 'pageParam' => 'online-page',
				 'params' => array_merge(Yii::$app->request->get(), ['tab' => 'tab-now-playing'])
			 ],
		]);
		
		return $this->render('online-user', [
			'currentUser'  => $this->_user ? $this->_user : null,
			'game'  => $this->_game,
			'userReadyProvider' => $userReadyProvider,
			'userNowPlayingProvider' => $userNowPlayingProvider,
			'userOnlineProvider' => $userOnlineProvider,
			'tab' => $tab
		]);
	}

	/**
	 * @return string
	 */
	public function actionChooseGame()
	{
		if ($slug = Yii::$app->request->get('slug')) {
			$game = Game::findOne(['slug' => $slug]);
			if ($game) {
				$this->_setWebUserGame($game);

				if ($this->_user) {
					$this->_user->current_game_id = $game->game_id;
					$this->_user->save();
				}

				if ($this->_user && !empty($this->_user->getUserIntegrationByGame($game->game_id))) {
					ActivityHelper::registerUserEvent(ActivityHelper::EVENT_TYPE_USER_JOIN, $game->game_id);
				}

				return $this->redirect(Url::toRoute(['den/index', 'gameUrl' => $game->slug]));
			}
		}

		return $this->render('choose_game', [
			'games' => Game::find()->orderBy(['sort' => SORT_ASC])->all()
		]);
	}

	/**
	 * Страница профиля игры пользователя
	 *
	 * @param string $gameUrl
	 * @param int    $userId
	 *
	 * @param string $tab
	 * @param bool $matchShow
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 */
	public function actionGameProfile ($gameUrl, $userId, $tab = 'game', $matchShow = false)
	{
		$params = [];
		$chat = null;

		$game = $this->_game;
		$user = User::find()
			->byUserId($userId)
			->with(
				[
					'userGames' => function (UserGameQuery $query) use ($game) {
						$query->byGameId($game->game_id);
					}
				]
			)
			->limit(1)
			->one();

		if (!$game) {
			throw new NotFoundHttpException('Игра не найдена');
		}
		if (!$user) {
			throw new NotFoundHttpException('Пользователь не найден');
		}

		$userGame = $user->userGames[$game->game_id] ?? null;

		if (!$userGame) {
			throw new NotFoundHttpException('Не найдена игра пользователя');
		}

		if ($this->_user && $userId != $this->_user->user_id) {
			$chat = TalksHelper::getCreateTalkBetween($this->_user->user_id, $userId);
		}
		
		$params = [
			'user' => $user,
			'game' => $game,
			'tab'  => $tab,
			'matchShow' => $matchShow,
			'chat' => $chat
		];
		
		if ($tab === 'game') {
			if ($url = ChangeGameHelper::checkUserGame(
				$this->_user,
				$game,
				Url::to(['user/game-profile', 'gameUrl' => $gameUrl, 'userId' => $userId])
			)
			) {
				return $this->redirect($url);
			}

			if (!$user->userGame) {
				throw new NotFoundHttpException('У пользователя нет профиля в этой игре');
			}

			\Yii::$app->user->setReturnUrl(Url::to());
		}

		return $this->render('profile-' . $tab, $params);
	}


	/**
	 * Вклдадка Турниры (профиля игры пользователя)
	 *
	 * @param string $gameUrl
	 * @param int $userId
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 */
	public function actionGameProfileTournaments($gameUrl, $userId){

		$game = $this->_game;
		$user = User::find()
			->byUserId($userId)
			->with([
				'userGames' => function (UserGameQuery $query) use ($game) {
					$query->byGameId($game->game_id);
				}
			])
			->limit(1)
			->one();

		if (!$game) {
			throw new NotFoundHttpException('Игра не найдена');
		}
		if (!$user) {
			throw new NotFoundHttpException('Пользователь не найден');
		}

		$userGame = $user->userGames[$game->game_id] ?? null;

		if (!$userGame) {
			throw new NotFoundHttpException('Не найдена игра пользователя');
		}

		if ($url = ChangeGameHelper::checkUserGame(
			$this->_user,
			$game,
			Url::to(['user/game-profile', 'gameUrl' => $gameUrl, 'userId' => $userId]))
		) {
			return $this->redirect($url);
		}

		if (!$user->userGame) {
			throw new NotFoundHttpException('У пользователя нет профиля в этой игре');
		}

		return $this->render(
			'profile-tournaments', [
			'user'        => $user,
			'game'        => $game,
		]);

	}
	
	/**
	 * Страница профиля пользователя
	 *
	 * @param null|int $id
	 *
	 * @return string
	 */
	public function actionProfile($id = null)
	{
		$uploadImageForm = new UploadImageForm();

		if ($id) {
			if ($id != $this->_user->user_id) {
				return $this->redirect(['/den/']);
			}
			$user = User::findOne($id);
		} else {
			$user = User::findOne($this->_user->user_id);
			if ($user) {
				return $this->redirect(['user/profile/' . $this->_user->user_id]);
			}
		}
		
		if (Yii::$app->request->isPost) {
			if ($this->__uploadImage($user, $uploadImageForm)) {
				return $this->refresh();
			} elseif (\Yii::$app->request->post('set_region')) {
				if ($this->__setRegion($user)) {
					return $this->refresh();
				}
			}
		}

		$profileEditForm = ProfileEditForm::loadFromUserProfile($user->userProfile);
		$pubgNicknameForm = PubgNicknameForm::loadFromUserProfile($user->userProfile);

		$twitchIntegration = $user->getTwitchIntegration();
		$discordIntegration = $user->getDiscordIntegration();

		$userTeams = $user->getTeams()->orderBy('game_id')->all();
		if (!empty($userTeams)) {
			$userTeams = TeamHelper::currentGameTeamsOnTop($userTeams, $user->current_game_id);
		}

		return $this->render(
			'profile',
			[
				'uploadImageForm'    => $uploadImageForm,
				'passwordChangeForm' => new PasswordChangeForm(),
				'user'               => $user,
				'twitchIntegration'  => $twitchIntegration ? $twitchIntegration : null,
				'discordIntegration' => $discordIntegration,
				'profileEditForm'    => $profileEditForm,
				'pubgNicknameForm'   => $pubgNicknameForm,
				'userTeams'          => $userTeams
			]
		);
	}
	
	/**
	 * Сохранение никанейма pubg
	 *
	 * @return Response
	 */
	public function actionAddPubgNickname()
	{
		$pubgNicknameForm = PubgNicknameForm::loadFromUserProfile($this->_user->userProfile);
		
		if (Yii::$app->request->isPost && $pubgNicknameForm->load(Yii::$app->request->post())) {
			$apiKey = \Yii::$app->params['pubgApiKey'];
			
			$nickname = $pubgNicknameForm->nickName;
			$userProfiles = UserProfile::find()
									   ->byPubgNickname($nickname)
									   ->limit(1)
									   ->exists();
			if ($userProfiles) {
				\Yii::$app->session->setFlash('error', 'Данный ник уже занят. Попробуйте указать другой');
				return $this->refresh();
			} else {
				$pubgClient = new Client($apiKey);
				try {
					$playerInfoByNickJSON = $pubgClient->getPlayersByName([$nickname]);
				} catch (\Exception $e) {
					throw $e;
				}
				
				$playerInfo = json_decode($playerInfoByNickJSON, true);
				if (!empty($playerInfo['errors']) && count($playerInfo['errors']) > 0) {
					\Yii::$app->session->setFlash('error', 'Данный ник не найден. Попробуйте указать другой');
					return $this->refresh();
				} else {
					$pubgNicknameForm->accountId = $playerInfo['data'][0]['id'] ?? null;
					if (empty($pubgNicknameForm->accountId)) {
						\Yii::$app->session->setFlash('error', 'Не удалось сохранить ник, попробуйте позже');
						return $this->refresh();
					}
				}
			}
			
			if ($pubgNicknameForm->save()) {
				\Yii::$app->session->setFlash('success', 'Изменения успешно сохранены');
			} else {
				$error = $pubgNicknameForm->getFirstError('nickName');
				
				\Yii::$app->session->setFlash('error', 'Не удалось сохранить изменения. ' . $error);
			}
		}
		
		return $this->redirect(['user/profile/' . $this->_user->user_id]);
	}

	/**
	 * Страница профиля пользователя
	 *
	 * @return string
	 */
	public function actionProfileEdit()
	{
		$user = $this->_user;
		if (!$user->userProfile->edit_profile) {
			$user->userProfile->edit_profile = UserProfile::PROFILE_EDITED;
			$user->userProfile->save();
		}

		$userProfileEditFrom = new UserProfileEditForm(
			[
				'user'            => $user,
				'firstName'       => $user->userProfile->first_name,
				'lastName'        => $user->userProfile->last_name,
				'middleName'      => $user->userProfile->middle_name,
				'birthDate'       => Yii::$app->formatter->asDate($user->userProfile->birthdate, 'php:d.m.Y'),
				'city'            => $user->userProfile->city,
				'country'         => $user->userProfile->country_id,
				'currentPhone'    => ($user->phone ? $user->phone : '+7'),
				'phone'           => ($user->phone ? $user->phone : '+7'),
				'currentNickname' => $user->nick_name,
				'nickname'        => $user->nick_name,
				'currentEmail'    => $user->email,
				'email'           => $user->email,
			]
		);

		if (Yii::$app->request->isPost && $userProfileEditFrom->load(Yii::$app->request->post())) {
			if ($userProfileEditFrom->save())
			{
				\Yii::$app->session->setFlash('success', 'Изменения успешно сохранены');
				return $this->redirect(['user/profile/' . $user->user_id]);
			}
		}

		return $this->render('profile_edit', [
			'userProfileEditFrom' => $userProfileEditFrom,
			'user' => $user
		]);
	}
	
	/**
	 * @return Response
	 */
	public function actionAddSocial()
	{
		$profileEditForm = ProfileEditForm::loadFromUserProfile($this->_user->userProfile);
		
		if (Yii::$app->request->isPost && $profileEditForm->load(Yii::$app->request->post())) {
			if ($profileEditForm->save()) {
				\Yii::$app->session->setFlash('success', 'Изменения успешно сохранены');
			} else {
				$error = $profileEditForm->getFirstError('vk_link');
				
				\Yii::$app->session->setFlash('error', 'Не удалось сохранить изменения. ' . $error);
			}
		}
		
		return $this->redirect(['user/profile/' . $this->_user->user_id]);
	}

	/**
	 * Рефералы
	 *
	 * @return string
	 */
	public function actionProfileReferredUsers()
	{
		$pageSize = 30;
		$query = User::find()
			->where(['referral_id' => $this->_user->user_id])
			->active();

		$count = $query->count();
		$pagination = new Pagination(['totalCount' => $count, 'pageSize' => $pageSize]);

		$users = $query->offset($pagination->offset)
					   ->limit($pagination->limit)
					   ->all();

		return $this->render('referrals', ['users' => $users, 'pagination' => $pagination]);
	}

	/**
	 * @param $userId
	 *
	 * @return string
	 * @throws ForbiddenHttpException
	 * @throws NotFoundHttpException
	 */
	public function actionReferredGameProfiles($userId)
	{
		$user = User::findOne($userId);

		if (!$user) {
			throw new NotFoundHttpException('Не удалось найти пользователя');
		}

		if ($user->referral_id != Yii::$app->user->identity->user_id) {
			throw new ForbiddenHttpException('Нет прав для просмотра этой страницы');
		}

		return $this->render('referred-game-profiles', ['user' => $user]);
	}

	/**
	 * Повторная отправка ссылки для подтверждения e-mail
	 * (отправляем не более, чем 1 за сессию на 1 e-mail)
	 */
	public function actionEmailConfirm()
	{
		$key = 'email-confirm' . $this->_user->email;
		if (!Yii::$app->session->get($key, false)) {
			$this->_user->sendEmailForConfirm();
		}
	}

	/**
	 * Отправка SMS для подтверждения номера телефона
	 */
	public function actionPhoneVerify()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		$result = ['status' => 'error', 'error' => ''];

		/** @var WebUser $user */
		$user = Yii::$app->user;
		$code = \Yii::$app->request->post('code') ?? null;
		$phone = $this->_user->phone;

		if ($code && $phone) {
			if ($user->checkSmsCode($code, $phone)) {
				$this->_user->is_phone_verified = 1;
				if ($this->_user->save()) {
					$result['status'] = 'ok';
					(new RewardHelper())->processReward(RewardHelper::REWARD_TYPE_PHONE_CONFIRM, $user->identity);
				} else {
					$result['error'] = 'Не удалось сохранить статус';
				}
			} else {
				$result['error'] = 'Код неверный';
			}
		} else {
			$result['error'] = 'Не обнаружен код или номер телефона';
		}

		return $result;
	}

	/**
	 * Подтверждение телефона по СМС. Отправка кода
	 *
	 * @return array
	 */
	public function actionPhoneRequestCode()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		/** @var WebUser $user */
		$user = Yii::$app->user;

		$phone = $this->_user->phone;

		$result = [
			'status' => 'ok',
			'message' => 'Код подтверждения успешно отправлен на Ваш телефон.',
		];

		if (!$phone) {
			$result['status']  = 'error';
			$result['message'] = 'Не указан номер телефона для подтверждения';
		} else {
			if (!$user->isSmsSent($phone) || $user->canRepeatSms($phone)) {
				$code    = $user->getSmsCode($phone, true);
				$message = 'Ваш код подтверждения: ' . $code;

				if (!Yii::$app->smsSender->send($phone, $message)) {
					$result['status']  = 'error';
					$result['message'] = Yii::$app->smsSender->getMessage() . Yii::$app->smsSender->getErrorCode();
				}
			} else {
				$result['status']  = 'ok';
				$result['message'] = 'Код уже был отправлен. Повторно отправить код на этот номер можно будет через ' . ceil($user->timeToRepeatSms($phone) / 60) . ' минут.';
			}

			if (YII_ENV_DEV) {
				$result['message'] .= ' ' . $user->getSmsCode($phone);
			}
		}

		return $result;
	}

	/**
	 * Смена пароля, ajax post
	 *
	 * @return array
	 */
	public function actionPasswordChange()
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

		$result = ['status' => 'error', 'error' => ''];

		$form = new PasswordChangeForm();
		if ($form->load(\Yii::$app->request->post()) && $form->validate()) {
			$this->_user->setPassword($form->newPassword);
			if ($this->_user->save()) {
				$result['status'] = 'ok';
			} else {
				$result['error'] = 'Не удалось сменить пароль';
			}
		} else {
			$result['error'] = 'Пожалуйста, проверьте правильность текущего пароля';
		}

		return $result;
	}

	/**
	 * Приглашения юзера в команду
	 *
	 * @return string
	 */
	public function actionTeamInvite()
	{
		$pageSize = 10;
		$userId = $this->_user->user_id;

		if (Yii::$app->request->get('invite_id') || Yii::$app->request->get('act')) {
			$id = intval(Yii::$app->request->get('invite_id'));
			$act = Yii::$app->request->get('act');
			if ($id && in_array($act, ['accept', 'refuse'])) {
				$invite = TeamInvitation::findOne($id);
				if ($invite && $invite->user_id == $userId) {
					$status = $act == 'accept'
						? TeamInvitation::STATUS_ACCEPTED
						: TeamInvitation::STATUS_REFUSED;
					if ($invite->status != $status) {
						$invite->status = $status;
						if ($invite->save()) {
							$gameSlug = $invite->team->game->slug;
							$teamUrl =
								Url::to(
									['team/profile', 'teamId' => $invite->team->team_id, 'gameUrl' => $gameSlug]
								);
							if ($invite->status == TeamInvitation::STATUS_ACCEPTED) {
								$teamUser               = new TeamUser();
								$teamUser->team_id      = $invite->team_id;
								$teamUser->user_game_id = $this->_user->userGame->user_game_id;


								if ($teamUser->save()) {
									\Yii::$app->session->setFlash(
										'success',
										'Приглашение успешно принято, добро пожаловать в команду
                                                 <a href="'.$teamUrl.'">' .
                                                        $invite->team->teamProfile->name .
                                                        '</a>!'
                                                    );

									return $this->redirect('/user/team-invite');
								} else {
									Yii::$app->session->setFlash('error', 'Не удалось добавиться в команду');
								}
							} else {
								\Yii::$app->session->setFlash(
									'success',
									'Вы отклонили приглашение вступить в команду
								 <a href="' . $teamUrl . '">' .
									$invite->team->teamProfile->name .
									'</a>'
								);

								return $this->redirect('/user/team-invite');
							}
						} else {
							Yii::$app->session->setFlash('error', 'Не удалось сохранить статус приглашения');
						}
					}
				} else {
					Yii::$app->session->setFlash('error', 'Приглашение не найдено');
				}
			}
		}

		$invites = TeamInvitation::find()
			 ->where(
				 [
					'status' => TeamInvitation::STATUS_SENT,
					'user_id' => $userId
				 ]
			);

		$count  = $invites->count();
		$pagination = new Pagination(['totalCount' => $count, 'pageSize' => $pageSize]);

		$invites = $invites
			->offset($pagination->offset)
			->limit($pagination->limit)
			->all();

		return $this->render('invites', [
			'invites' => $invites,
			'pagination' => $pagination
		]);
	}

	/**
	 * Запрос на вывод денег
	 *
	 * @param null $requestId
	 * @param bool $accepted
	 *
	 * @return string|Response
	 * @throws ForbiddenHttpException
	 */
	public function actionMoneyWithdrawal($requestId = null, $accepted = false)
	{
		if (!$this->_user->is_adult) {
			throw new ForbiddenHttpException('Доступ запрещен');
		}

		$withdrawalModel = new MoneyWithdrawalForm();
		$withdrawalModel->userBalance = $this->_user->account->balance;

		if (Yii::$app->request->isPost && $withdrawalModel->load(Yii::$app->request->post()) && $withdrawalModel->validate()) {
			$payoutRequest = new PayoutRequest([
				'account_id' => $this->_user->account->account_id,
				'sum' => $withdrawalModel->amount,
				'to' => PayoutRequest::PAYMENT_SYSTEM_QIWI,
				'phone' => $this->_user->phone,
				'status' => PayoutRequest::STATUS_NEW,
				'date_status_set' => time()
			                                   ]);
			if (!$payoutRequest->save()) {
				Yii::error($payoutRequest->getFirstErrors());
				\Yii::$app->session->setFlash('error', 'Не удалось создать запрос');
				return $this->redirect(['user/bank']);
			}

			return $this->render('money-withdrawal', [
				'amountPlan' => $withdrawalModel->amount,
				'amountReal' => 0.97 * $withdrawalModel->amount,
				'requestId' => Crypt::get()->encrypt($payoutRequest->payout_request_id),
				'phone' => $this->_user->phoneFormatted]);
		}

		if ($requestId) {
			$id = Crypt::get()->decrypt($requestId);
			$payoutRequest = PayoutRequest::findOne($id);

			if (!$payoutRequest) {
				return $this->redirect(['user/bank']);
			}

			if ($accepted) {
				$payoutRequest->setStatus(PayoutRequest::STATUS_PENDING);
				try {
					(new BillingHelper())->processMoneyWithdrawal($this->_user, $payoutRequest->sum);
				} catch (Exception $exception) {
					\Yii::$app->session->setFlash('error', $exception->getMessage());
				}
			} else {
				$payoutRequest->setStatus(PayoutRequest::STATUS_CANCELED);
			}
		}

		return $this->redirect(['user/bank']);
	}

	/**
	 * Загрузка изображений
	 *
	 * @param User $user
	 * @param UploadImageForm $uploadImageForm
	 *
	 * @return bool
	 */
	private function __uploadImage($user, $uploadImageForm)
	{
		if (Yii::$app->request->isPost) {
			$uploadImageForm->imageFile = UploadedFile::getInstance($uploadImageForm, 'imageFile');
			if ($imageName = $uploadImageForm->upload($user->userProfile->getImagePath(), UploadImageForm::TYPE_USERS)) {
				$user->userProfile->profile_image = $imageName;
				$user->userProfile->save();
				(new RewardHelper())->processReward(RewardHelper::REWARD_TYPE_AVATAR_UPLOAD, $user);
				return true;
			}
		}

		return false;
	}

	/**
	 * Установка региона для интеграции (пока только Battle.net)
	 *
	 * @param User $user
	 *
	 * @return bool
	 */
	private function __setRegion(User $user)
	{
		$gameId = intval(Yii::$app->request->post('game_id'));
		$region = Yii::$app->request->post('region');

		if ($game = Game::findOne($gameId)) {
			if ($game->integration_id == Integration::BATTLE_NET_BASIC) {
				if ($integration = $user->getUserIntegrationByGame($gameId)) {
					$integration->integrationModel->region = $region;
					if ($integration->integrationModel->save()) {
						\Yii::$app->session->setFlash('success', 'Регион для '. $game->title .' успешно установлен в ' . $region);
					} else {
						\Yii::$app->session->setFlash('error', 'Не удалось сменить регион. Обратитесь, пожалуйста, в <a target="_blank" href="https://support.go2den.com/">поддержку</a>.');
					}

					return true;
				}
			}
		}

		return false;
	}
}
